export const AUTH = "AUTH";

export const authorize = (email, token) => dispatch => {
  localStorage.setItem("access_token", token);

  dispatch({
    type: AUTH,
    payload: email
  });
};

