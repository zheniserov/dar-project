import { AUTH } from "../actions/auth.action";

const initialState = {};


export default function (state = initialState, action){
    switch(action.type){
        case AUTH:
            return{
                ...state, 
                email: action.payload,
                
            };
        default:
            return state;
    }
}