import React, { Component } from 'react';
import AddPosts from "./addPosts/addPosts";
import Post from "./addPosts/post";
import "./posts.scss";
var num;
export default class Posts extends Component {

    state = {
        posts: []
    };

    clickHandler = () => {
        window.localStorage.clear();
        this.props.history.push("/auth");
    }

    

    addPosts = post => {
        this.setState(currentState => ({
            posts: [...currentState.posts, post]
        }));
    };

    deletePost = index => {
        this.setState(state => {
            state.posts.splice(index, 1);
            return { posts: state.posts };
        });
    };

    editPost = post => {
        this.setState(state => {
            state.posts.splice(num, 1, post);
            console.log(state.posts);
            this.hidePost(num);
        });
    };

    hidePost = index => {
        const post = document.getElementsByClassName("App__post__content");
        const edit = document.getElementsByClassName("App__edit__content");

        if (post[index].style.display === "none") {
            post[index].style.display = "flex";
            edit[index].style.display = "none";
        } else {
            post[index].style.display = "none";
            edit[index].style.display = "flex";
        }
        num = index;
    };
    render() {
        const data = window.sessionStorage.getItem('email');
        const email = data ? data.split("@")[0] : "";
        const { posts } = this.state;

        return (

            <div>
                <div className="Header__home" >
                    <h1>Posts</h1>
                    <div className="Header__home--buttons">
                        <button >{email}</button>
                        <button onClick={this.clickHandler} >Logout</button>
                    </div>
                </div>
                <div className="App-header">
                    <div className="App__content">
                        <div className="App">
                            <AddPosts addAction={this.addPosts} />
                            <Post
                                list={posts}
                                deletePost={this.deletePost}
                                hidePost={this.hidePost}
                                editPost={this.editPost}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}