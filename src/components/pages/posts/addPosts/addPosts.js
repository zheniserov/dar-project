import React, { Component } from 'react';

import "./TodoList.scss";


class addPosts extends Component {
    state = {
        title: "",
        inputValue: ""
    };


    onInputChange = (event, name) => {
        const newValue = {};
        newValue[name] = event.target.value;
        this.setState({ ...newValue });
    };

    handleSubmit = event => {
        event.preventDefault();

        const newPost = {
            title: this.state.title,
            text: this.state.inputValue
        };
        this.props.addAction(newPost);
        this.setState({ title: "", inputValue: "" });
    };
    render() {
        const { title, inputValue } = this.state;
        return (
            <div className="Add__post">
                <h1>Add Post</h1>
                <form className="App__form" onSubmit={this.handleSubmit}>
                    <input
                        className="App__input"
                        value={title}
                        name="title"
                        onChange={event => this.onInputChange(event, 'title')}
                        placeholder="Title: "
                        required
                    />
                    <textarea
                        className="App__input"
                        value={inputValue}
                        name="post"
                        onChange={event => this.onInputChange(event, 'inputValue')}
                        placeholder="Body: "
                        required
                    />
                    <button className="App__form__button">Add post</button>
                </form>
            </div>
        );
    }
}

export default addPosts;