import React, { Component } from 'react';

import "./TodoList.scss";
import deleteImage from "../../../../assets/img/delete.svg";
import editImage from "../../../../assets/img/edit.svg";
import checkImage from "../../../../assets/img/check.svg";
import backImage from "../../../../assets/img/delete-button.svg";
class Post extends Component {
    state = {
        title: "",
        inputValue: ""
    };

    toggleEditing = (event, name) => {
        const newValue = {};
        newValue[name] = event.target.value;
        this.setState({ ...newValue });
    };

    handleEdit = event => {
        event.preventDefault();
        const editedPost = {
            title: this.state.title,
            inputValue: this.state.inputValue,
        };
        this.props.editPost(editedPost);
        this.setState({ title: "", inputValue: "" });
    };
    render() {
        const { list, deletePost, hidePost } = this.props;
        return (
            <div className="App__post">
                {list.map((post, index) => (

                    <div key={index}>
                        <div className="App__post__content">
                            <div className="App__post__content--post">
                                <div>
                                    <div className="App__post__content--title">
                                        <h2>{post.title}</h2>
                                    </div>
                                    <div className="App__post__content--body">
                                        <p>{post.text}</p>
                                    </div>
                                </div>

                                <div className="App__actions">
                                    <button onClick={() => { hidePost(index); }} >
                                        <img
                                            className="App__actions--icons"
                                            alt="edit"
                                            src={editImage}
                                        />
                                    </button>
                                    <button onClick={() => { deletePost(index); }} >
                                        <img
                                            className="App__actions--icons"
                                            alt="delete"
                                            src={deleteImage}
                                        />
                                    </button>
                                </div>
                            </div>

                        </div>

                        <div className="App__edit__content">
                            <form className="Edit__form" onSubmit={this.handleEdit}>
                                <div className="App__edit--content">
                                    <input
                                        className="App__edit--title"
                                        value={this.state.title}
                                        onChange={event => this.toggleEditing(event, "title")}
                                        required
                                    />
                                    <textarea
                                        className="App__edit--body"
                                        value={this.state.inputValue}
                                        onChange={event => this.toggleEditing(event, "inputValue")}
                                        required
                                    />
                                </div>
                                <div className="App__actions">
                                    <button onClick={this.handleEdit}>
                                        <img
                                            className="App__edit--icons"
                                            alt="checked"
                                            src={checkImage}
                                        />
                                    </button>
                                    <button onClick={() => { hidePost(index); }} >
                                        <img
                                            className="App__edit--icons"
                                            alt="back"
                                            src={backImage}
                                        />
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                ))}
            </div>
        );
    }
}

export default Post;