import React, { Component } from 'react';
import { getUsers } from '../../../services/users'

import './users.scss';
import xb from'../../../assets/img/delete.svg';
import sqb from'../../../assets/img/edit.svg';
export default class Users extends Component {

    state = {
        users: []
    };


    clickHandler = () => {
        window.localStorage.clear();
        this.props.history.push("/auth");
    }

    componentDidMount() {
        getUsers()
            .then(response => {
                this.setState({ users: response.data.data });
            })
            .catch(error => {
                console.log(error);
            })
    }

    render() {
        const data = window.sessionStorage.getItem('email');
        const email = data ? data.split("@")[0] : "";
        return (
            <div>
                <div className="Header__home" >
                    <h1>Users page</h1>
                    <div className="Header__home--buttons">
                        <button >{email}</button>
                        <button onClick={this.clickHandler} >Logout</button>
                    </div>                </div>
                <div className="Users__data">
                    <div className="Users__data--name">
                        <span>ID</span>
                        <span>Email</span>
                        <span>First Name</span>
                        <span>Last Name</span>
                        <span>Avatar</span>
                        <span>Action</span>
                    </div>
                    {this.state.users.map(user => (
                        <div key={user.id} className="Users__data--data">
                            <p>{user.id}</p>
                            <p>{user.email}</p>
                            <p>{user.first_name}</p>
                            <p>{user.last_name}</p>
                            <p><img src={user.avatar} alt="" /></p>
                            <div className="Users__data--buttons">
                                <img src={sqb} alt=""/>
                                <img src={xb} alt=""/>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}