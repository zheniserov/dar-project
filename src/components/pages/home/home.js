import React, {Component} from 'react';
//import {Link} from 'react-router-dom';

//import { connect } from "react-redux";
// import {changeUsername} from "../../../store/actions/auth.action";

import "./home.scss";

class Home extends Component {

    

    clickHandler = () => {
        window.localStorage.clear();
        this.props.history.push("/auth");
    }
    render(){
        const data = window.sessionStorage.getItem('email');
        const email =  data ? data.split("@")[0] : "";
        return (

            <div className="Header__home" >
                <h1>Home page</h1>
                <div className="Header__home--buttons">
                    <button >{email}</button>
                    <button onClick={this.clickHandler} >Logout</button>
                </div>
    
    
            </div>
        )
    }
}


export default Home;