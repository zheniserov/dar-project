import React from 'react';
import { NavLink } from 'react-router-dom';

import './header.scss';

export default function Header() {
    return (
        <div>
            <div className="Header__name">
                <h1>Dar project</h1>
            </div>
            <div className="Header">
                <NavLink exact className="Header__link" to="/">Home</NavLink>
                <NavLink className="Header__link" to="/users">Users</NavLink>
                <NavLink className="Header__link" to="/posts">Posts</NavLink>
            </div>
        </div>
    );
}