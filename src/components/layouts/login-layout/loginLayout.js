import React, { Component } from 'react';
import { Formik } from "formik";
import Input from "../../shared/input/Input";

import { connect } from "react-redux";
import { login } from "../../../services/login";
import { authorize } from "../../../store/actions/auth.action";

import "./loginLayout.scss";

class LoginLayout extends Component {

    state = {
        formError: ""
    };


    componentDidMount() {
        const token = localStorage.getItem("access_token");
        if (token) {
            this.props.history.push("/");
        }
    }


    handleSubmit = values => {
        const { email, password } = values;

        var data = {
            email: email, 
            password: password
        }

        login(data)
            .then(response => {
                this.props.authorize(email, response.data.token);
                window.sessionStorage.setItem('email', email);
                this.props.history.push("/")
            })
            .catch(error => {
                if (error.response) {
                    this.setState({ formError: error.response.data.error });
                } else {
                    this.setState({ formError: error.message });
                }
            });

    }

    renderForm = ({
        handleSubmit,
        handleChange,
        errors,
        setFieldTouched,
        touched
    }) => (
            <form onSubmit={handleSubmit}>
                <Input
                    name="email"
                    type="text"
                    placeholder="Email: "
                    onBlur={() => setFieldTouched("email")}
                    error={errors.email}
                    touched={touched.email}
                    onChange={handleChange}
                />
                <Input
                    name="password"
                    type="password"
                    placeholder="Password: "
                    onBlur={() => setFieldTouched("password")}
                    error={errors.password}
                    touched={touched.password}
                    onChange={handleChange}
                />
                {this.state.formError && (
                     <p className="text--error" > 
                     {/* {this.state.formError}  */}
                     </p>
                )}
                <div className="Register__button" >
                    <button type="submit" >Login</button>
                    <h2>Not yet registered? &nbsp; <span onClick={this.handleClick}>Register</span></h2>
                </div>
            </form>
        )

    handleClick = event => {
        event.preventDefault();
        this.props.history.push("/register")
    }

    render() {
        return (
            <div className="Login">
                <div className="Login__inner">
                    <div className="Login__name">
                        <h1>Authorization</h1>
                    </div>
                    <Formik
                        onSubmit={this.handleSubmit}
                        render={this.renderForm}
                        validate={this.renderForm}
                        initialValues={{
                            email: "",
                            password: ""
                        }}
                    />
                </div>
            </div>
        );
    }
}

export default connect(
    null,
    { authorize }
)(LoginLayout);