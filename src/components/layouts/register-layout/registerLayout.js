import React, { Component } from 'react';
import { Formik } from "formik";
import Input from "../../shared/input/Input";

// import { useToasts } from 'react-toast-notifications';

import { connect } from "react-redux";
import { register } from "../../../services/auth";
import { authorize } from "../../../store/actions/auth.action";

import "./registerLayout.scss";

class RegisterLayout extends Component {

    state = {
        formError: ""
    };

    componentDidMount() {
        const token = localStorage.getItem("access_token");
        if (token) {
            this.props.history.push("/");
        }
    }

    handleSubmit = values => {
        const { email, password } = values;

        register(email, password)
            .then(response => {
                this.props.authorize(email, response.data.token);
                window.sessionStorage.setItem('email', email);
                this.props.history.push("/");
            })
            .catch(error => {
                if (error.response) {
                    this.setState({ formError: error.response.data.error });
                } else {
                    this.setState({ formError: error.message });
                }
            });

    }

    validateForm = values => {
        const errors = {};

        const emailRegExp = new RegExp(
            [
                '^(([^<>()[\\]\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\.,;:\\s@"]+)*)',
                '|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.',
                "[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+",
                "[a-zA-Z]{2,}))$"
            ].join("")
        );

        if (!values.username) {
            errors.username = "Input username";
        }

        if (!values.email) {
            errors.email = "Input Email";
        } else if (!emailRegExp.test(values.email)) {
            errors.email = "Input valid Email";
        }

        if (!values.password) {
            errors.password = "Input password";
        } else if (values.password.length <= 6) {
            errors.password = "Password should contain at least 6 symbols";
        }

        if (!values.rePassword) {
            errors.rePassword = "Repeat password";
        } else if (values.password !== values.rePassword) {
            errors.rePassword = "Passwords do not match";
        }

        return errors;
    };

    renderForm = ({
        handleSubmit,
        handleChange,
        errors,
        setFieldTouched,
        touched
    }) => (
            <form onSubmit={handleSubmit}>
                <Input
                    name="username"
                    type="text"
                    placeholder="Username: "
                    onBlur={() => setFieldTouched("username")}
                    error={errors.username}
                    touched={touched.username}
                    onChange={handleChange}
                />
                <Input
                    name="email"
                    type="text"
                    placeholder="Email: "
                    onBlur={() => setFieldTouched("email")}
                    error={errors.email}
                    touched={touched.email}
                    onChange={handleChange}
                />
                <Input
                    name="password"
                    type="password"
                    placeholder="Password: "
                    onBlur={() => setFieldTouched("password")}
                    error={errors.password}
                    touched={touched.password}
                    onChange={handleChange}
                />
                <Input
                    name="rePassword"
                    type="Password"
                    placeholder="Repeat password: "
                    onBlur={() => setFieldTouched("rePassword")}
                    error={errors.rePassword}
                    touched={touched.rePassword}
                    onChange={handleChange}
                />
                {this.state.formError && (
                    <p className="text--error" > {this.state.formError} </p>
                )}
                <div className="Register__button" >
                    <button type="submit" >Sign up</button>
                    <h2>Do you have an account? &nbsp; <span onClick={this.handleClick}>Login</span></h2>
                </div>
            </form>
        )

    handleClick = event => {
        event.preventDefault();
        this.props.history.push("/auth")
    }

    render() {
        return (
            <div className="Register">
                <div className="Register__inner">
                    <div className="Register__name">
                        <h1>Sign up</h1>
                    </div>
                    <Formik
                        onSubmit={this.handleSubmit}
                        render={this.renderForm}
                        validate={this.renderForm}
                        initialValues={{
                            username: "",
                            email: "",
                            password: "",
                            rePassword: ""
                        }}
                    />
                </div>
            </div>
        );
    }
}

export default connect(
    null,
    { authorize }
)(RegisterLayout);