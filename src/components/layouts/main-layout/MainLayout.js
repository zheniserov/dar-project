import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';

import Header from '../../shared/header/header';
import Home from '../../pages/home/home';
import Users from '../../pages/users/users';
import Posts from '../../pages/posts/posts';

import './mainLayout.scss';

export default class MainLayout extends Component{

    componentDidMount(){
        const token = localStorage.getItem("access_token");
        if(!token){
            this.props.history.push("/auth");
        }
    }

    render(){
        return(
            <div className="Body">
                <div className="Head">
                    <Header/>
                </div>
                <div className="Components">
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/users" component={Users}/>
                        <Route path="/posts" component={Posts}/>
                    </Switch>
                </div>
            </div>
        );
    }
}