import api from "./api";

if (localStorage.getItem("access_token")) {
  api.defaults.headers.common["Authorization"] = `Bearer ${localStorage.getItem(
    "access_token"
  )}`;
}

export function register(email, password) {
  return api.post("/register", { email, password });
}
