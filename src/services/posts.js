import api from './api';

export function getPosts(){
    return api.get('/posts');
}