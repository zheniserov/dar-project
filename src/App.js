import React, { Component } from 'react';
import { Route, Switch } from "react-router-dom";
import MainLayout from './components/layouts/main-layout/MainLayout';
import { ToastProvider } from 'react-toast-notifications';


import './components/style/reset.css';
import LoginLayout from './components/layouts/login-layout/loginLayout';
import RegisterLayout from './components/layouts/register-layout/registerLayout';
export default class App extends Component {
  render() {
    return (
      <ToastProvider>
        <Switch>
          <Route path="/auth" component={LoginLayout} />
          <Route path="/register" component={RegisterLayout} />
          <Route path="/" component={MainLayout} />
        </Switch>
      </ToastProvider>
    );
  }
}

